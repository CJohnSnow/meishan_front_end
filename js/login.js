$(function() {
				layui.use(['form','layer'], function() {
					var form = layui.form;
					var layer = layui.layer;
					var LoginUrl;
					
					form.on('submit(login)', function(data) {
						var TheRole = data.field.role,
						    workId = data.field.username,
							password = data.field.password;
								if (TheRole == 0) {
									LoginUrl = "v1/user/login";
									Ajax(LoginUrl,workId,password,0);
								} else {
									LoginUrl = "v1/user/login"
									Ajax(LoginUrl,workId,password,1);
								}
						return false;
					});

					function Ajax(Url,workId,password,type) {
						$.ajax({
							url: app.ServerUrl + Url,
							type: "get",
							contentType: "application/json;charset=UTF-8",
							dataType:"json",
							async:true,
							headers: {
								"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
							},
							data: {
								workId: workId,
								password: password,
								type:type
							},
							success: function(data) {
								// console.log(data);
								if(data.resultStatus == 1){
									var Info = data.objs[0]
									if (type == 0) {
										app.setUserGlobalInfo(Info);
									}else {
										app.setManagerGlobalInfo(Info)
									}
									layer.msg('登录成功！',{icon:6});

									setTimeout(function(){
										window.location.href = '../Index.html'
									},1200)
								}else {
									layer.msg(data.resultIns,{icon:5});
								}
							},
							error: function(e) {
								console.log(e)
								layer.open({
									content: e,
									btnAlign: 'c',
									anim: 5,
									icon: 5,
									cancel: function() {}
								});
							}

						})
					}

				});



			})