window.app = {

	/**
	 * 公共接口
	 */
	ServerUrl: 'http://60.205.182.15:8080/inventory/',
	// ServerUrl: 'http://localhost:8080/',

    /**
	 * @param {Object} time
	 * 时间格式化,使用之前需要引入<script src="https://cdn.bootcss.com/moment.js/2.23.0/moment.min.js
		" type="text/javascript" charset="utf-8"></script>
	 */
    getFormatDate: function (time) {
			return moment(time).format('YYYY-MM-DD')
    },


	/**
	 * 保存用户的全局对象
	 * @param {Object} user
	 */
	setUserGlobalInfo: function(user) {
		var userInfoStr = JSON.stringify(user);
		localStorage.setItem("userInfo", userInfoStr);
	},
	
	setManagerGlobalInfo: function(user) {
		var userInfoStr = JSON.stringify(user);
		localStorage.setItem("ManagerInfo", userInfoStr);
	},
	
	/**
	 * 获取用户的全局对象
	 */
	getUserGlobalInfo: function() {
		var userInfoStr = localStorage.getItem("userInfo");
		return JSON.parse(userInfoStr);
	},
	getManagerGlobalInfo: function() {
		var userInfoStr = localStorage.getItem("ManagerInfo");
		return JSON.parse(userInfoStr);
	},

	/**
	 * 获取当前登录用户的对应管理员
	 */
	getManagerId: function() {
		var managerInfo = this.getManagerGlobalInfo();
		var userInfo = this.getUserGlobalInfo();
		if(managerInfo!=null){
			return managerInfo.managerId;
		}
		if(userInfo!=null){
			return userInfo.managerId;
		}
	},

	/**
	 * 获取当前登录用户的对应sessionId
	 */
	getSessionId: function() {
		var managerInfo = this.getManagerGlobalInfo();
		var userInfo = this.getUserGlobalInfo();
		if(managerInfo!=null){
			return managerInfo.sessionId;
		}
		if(userInfo!=null){
			return userInfo.sessionId;
		}
	},

	/**
	 * 给index.html 的vue 做权限初始化判断
	 * @returns {{material_allot: string, material_inStorage: string, user_operate: string, permission_operate: string, material_in_out: string, material_outStorage: string, role_operate: string, manager_operate: string, material_operate: string, people: string}}
	 */
	getPermissionData: function(){
		var managerInfo = this.getManagerGlobalInfo();
		var userInfo = this.getUserGlobalInfo();
		var Info = null;
		if(managerInfo!=null){
			Info =  managerInfo;
		}else if(userInfo!=null){
			Info =  userInfo;
		}else {
			return;
		}

		var permission_arr = [];
		for (let i = 0; i < Info.permissionRecordPoList.length; i++) {
			permission_arr.push(Info.permissionRecordPoList[i].permission)
		}

		// console.log(permission_arr);
		var permission_data =  {
			user_operate: "user:operate",
			manager_operate: 'manager:operate',
			material_inStorage: 'material:inStorage',
			material_outStorage: 'material:outStorage',
			material_operate: 'material:operate',
			material_allot: 'material:allot',
			permission_operate: 'permission:operate',
			role_operate: 'role:operate',

			//复合判断
			people: '',//user_operate&manager_operate
			material_in_out: '',//material_inStorage&material_outStorage
		};


		//单个权限判断
		for (let key in permission_data) {
			if(permission_arr.indexOf(permission_data[key])!=-1){
				permission_data[key] = true;
			}else {
				permission_data[key] = false;
			}
		}
		//复合权限判断
		permission_data['people'] = permission_data['user_operate']  || permission_data['manager_operate'] ;
		permission_data['material_in_out']  = permission_data['material_inStorage']  || permission_data['material_outStorage'] ;

		// console.log(permission_data);

		//个人用户不能编辑公告、处理报修
		if (userInfo!=null && userInfo.state!=0){
			permission_data['feedback'] = true
		}else {
			permission_data['feedback'] = false
		}

		return permission_data;
	},


	
	/**
	 * 登出后，移除用户全局对象
	 */
	userLogout: function() {
		plus.storage.removeItem("userInfo");
	},
	ManagerLogout: function() {
		plus.storage.removeItem("ManagerInfo");
	},

	/**
	 * 获取当前页面用户的id
	 */
	UrlSearch: function() {
		var name, value;
		var str = location.href; //获取到整个地址
		var num = str.indexOf("?")
		str = str.substr(num + 1); //取得num+1后所有参数，这里的num+1是下标 str.substr(start [, length ]
		var arr = str.split("&"); //以&分割各个参数放到数组里
		for (var i = 0; i < arr.length; i++) {
			num = arr[i].indexOf("=");
			if (num > 0) {
				name = arr[i].substring(0, num);
				value = arr[i].substr(num + 1);
				this[name] = value;
			}
		}
	},
	
	// 截取逗号后面的字符
	getCaption: function(obj){
	    var index=obj.lastIndexOf("\,");
	    obj=obj.substring(index+1,obj.length);
	    return obj;
	}
};

$(document).ajaxSend(function(event, jqxhr, settings) {
	jqxhr.setRequestHeader('Authorization', app.getSessionId())
});
