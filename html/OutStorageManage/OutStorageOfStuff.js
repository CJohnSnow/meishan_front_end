layui.use(['laydate', 'form','layer'], function() {
	var form = layui.form;
	var layer = layui.layer;
	var table_tby = document.getElementById("table-tby");
	var Tr = document.getElementById("Tr");
	var category = document.getElementById("category");
	var brand = document.getElementById("brand");
	var select_button_div = document.getElementById("select_button_div");
	var opposite = document.getElementById("opposite");
	var find_people = document.getElementById("find_people");
	var repository_opposite_div = document.getElementById("repository_opposite_div");
	var repository_opposite = document.getElementById("repository_opposite");
	var OutStorage_num = document.getElementById("OutStorage_num");
	var OutStorage_button = document.getElementById("OutStorage_button");
	var typical = document.getElementById("typical");
	var model = document.getElementById("model");
	var workId = document.getElementById("workId");
	var RepositoryDiv = document.getElementById("repository");
	var repository = app.getUserGlobalInfo();
	var repository_List = repository.repositoryList;
	var typeUrl;

	// 获取category的select
	$.ajax({
		url: app.ServerUrl + 'v1/type/getAllCategory',
		type: "get",
		contentType: "application/json;charset=UTF-8",
		dataType: "json",
		async: true,
		headers: {
			"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
		},
		data: {},
		success: function(data) {
			// console.log(data.objs[0])
			var typeData = data.objs[0];
			var typeHtml = '<option>请选择种类</option>';
			var repositoryHtml = '<option>请选择仓库</option>';
			
				typeHtml +=
					'<option value="'+ typeData[2].categoryId +'">' +
					typeData[2].categoryName +
					'</option>'
					
			
			category.innerHTML = typeHtml;
			for (let i = 0; i < repository_List.length; i++) {
				repositoryHtml +=
					'<option value="' + repository_List[i].repositoryId + '">' +
					repository_List[i].repositoryName +
					'</option>'
			}
			RepositoryDiv.innerHTML = repositoryHtml;
			form.render("select")
		},
		error: function(e) {
			console.log(e.statusText)
		}

	})


	// category选择后渲染type
	form.on('select(category)', function(data) {
		var managerId = app.getManagerId()
		let SelectValue = data.value;
		localStorage.setItem("OutStorage_categoryId",SelectValue);
			typeUrl = 'v1/type/getTypicalNameByCategoryId/3?managerId=' + managerId;
			Ajax_category(typeUrl);

	});

	// category的ajax
	function Ajax_category(Url) {
		$.ajax({
			url: app.ServerUrl + Url,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs)
				var typeData = data.objs[0];
				var typeHtml = "<option>请选择类型</option>";
				var typicalNameArray = [];
				for (let i = 0; i < typeData.length; i++) {
					if (typicalNameArray.indexOf(typeData[i]) != -1){
						continue;
					}
					typicalNameArray.push(typeData[i]);
				}
				for (var item in typicalNameArray){
					typeHtml += '<option value="' + typicalNameArray[item]+ '" >' +
						typicalNameArray[item] +
						'</option>'
				}
				typical.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

	// brand选择后渲染typical
	form.on('select(typical)', function(data) {
		// console.log(data.value)
		let SelectValue = data.value;
		Ajax_typical(SelectValue);
		// typical选择后渲染model
		form.on('select(brand)', function(data) {
			// console.log(data.value)
			// 使用brand的名字来进行渲染model
			var brand = data.value;
			Ajax_brand(SelectValue);
			form.on('select(model)', function(data) {
				// console.log(data)
				var model = data.value;
			});
		});
	});
	
	// repository的select
	form.on('select(repository)', function(data) {
		// console.log(data)
		var repository = data.value;
	});
	
	// select_button的按钮
	form.on('submit(select_button)',function(data){
		var Info = data.field;
		var user_Info = app.getUserGlobalInfo();
		opposite.classList.remove("layui-hide");
		$.ajax({
			url: app.ServerUrl + 'v1/repository/getThingsInRepositoryByCondition',
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {
				categoryId :Info.category,
				repositoryId : Info.repository_mine,
				typicalId:Info.typicalId,
				userId :user_Info.userId
			},
			success: function(data) {
				// console.log(data.objs[0])
				var typeData = data.objs[0];
				var typeHtml = "";
				for (let i = 0; i < typeData.length; i++) {
					typeHtml += 
					'<tr>'+
					'<td> ' + typeData[i].name + ' </td>  <' +
					'td > '+ typeData[i].categoryName +' </td>  <' +
					'td > '+ typeData[i].typicalName +' </td> <' +
					'td > '+ typeData[i].brand +' </td> <' +
					'td > '+ typeData[i].typicalModel +' </td>' +
					'<td > '+ typeData[i].inRepository +' </td>' +
					'</tr>'
				}
				table_tby.innerHTML = typeHtml;
			},
			error: function(e) {
				console.log(e.statusText)
			}
		
		})
	})

	// typical的ajax
	function Ajax_typical(typicalName) {
		var categoryId = localStorage.getItem("OutStorage_categoryId")
		var managerId = app.getManagerId()
		$.ajax({
			url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
				+ typicalName +"?managerId="+managerId +"&categoryId="+categoryId,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs[0])
				var typeData = data.objs[0];
				var typeHtml = "<option>请选择品牌</option>";
				for (let i = 0; i < typeData.length; i++) {
					typeHtml += '<option value="' + typeData[i].brand + '">' +
						typeData[i].brand +
						'</option>'
				}
				brand.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

	// brand的ajax
	function Ajax_brand(typicalName) {
		var categoryId = localStorage.getItem("OutStorage_categoryId")
		var managerId = app.getManagerId()
		$.ajax({
			url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
				+ typicalName +"?managerId="+managerId +"&categoryId="+categoryId,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs[0])
				var typeData = data.objs[0];
				var typeHtml = "<option>请选择型号</option>";
				for (let i = 0; i < typeData.length; i++) {
					var Arry = typeData[i];
					for (let j = 0; j < Arry.modelPo.length; j++) {
						typeHtml += '<option  value="' + Arry.modelPo[j].typicalId +'">' +
							Arry.modelPo[j].typicalModel +
							'</option>'
					}
				}
				model.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}
	
	form.on("submit(find_people)",function(data){
		var Info = app.getUserGlobalInfo();
		var workId = data.field.workId;
		$.ajax({
			url: app.ServerUrl + '/v1/repository/getRepositoryInfoByWorkId' ,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {
				tempUserId :Info.userId,
				workId :workId
			},
			success: function(data) {
				// console.log(data.objs[0])
				if (data.resultStatus == 1){
					var typeData = data.objs[0];
					var typeHtml = "<option>请选择此人的仓库</option>";
					for (let i = 0; i < typeData.length; i++) {
						var Arry = typeData[i];
						typeHtml += '<option x-test= "123" value="' + Arry.repositoryId +'">' +
							Arry.repositoryName +
							'</option>'
					}
					repository_opposite.innerHTML = typeHtml;
					form.render("select")
					repository_opposite_div.classList.remove("layui-hide");
				}else {
					layer.open({
						content: data.resultIns,
						btnAlign: 'c',
						anim: 5,
						icon: 5,
						cancel: function() {
						}
					});
				}

			},
			error: function(e) {
				layer.open({
					content: data.resultIns,
					btnAlign: 'c',
					anim: 5,
					icon: 5,
					cancel: function() {
					}
				});
			}
		})
	})
	
	// repository的select
	form.on('select(repository_opposite)', function(data) {
		OutStorage_num.classList.remove("layui-hide");
		OutStorage_button.classList.remove("layui-hide");
		var OutStorage_Info = data.value;
	});
	
	form.on('submit(search)',function(data){
		// console.log(data.field);
		var result = data.field;
		var Info = localStorage.getItem("userInfo");
		$.ajax({
			url: app.ServerUrl + 'v1/material/outStorageMaterial/' + result.category,
			type: "post",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data:JSON.stringify({
				count: result.outstorage_num,
				materialTypicalId: result.typicalId,
				operator: Info.userId,
				sourceRepositoryId: result.repository_mine,
				targetRepositoryId: result.repository_opposite,
				name:result.outstorage_num_name
			}),
			success: function(data) {
				// console.log(data)
				$.ajax({
					url: app.ServerUrl + 'v1/repository/getThingsInRepositoryByCondition',
					type: "get",
					contentType: "application/json;charset=UTF-8",
					dataType: "json",
					async: true,
					headers: {
						"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
					},
					data: {
						categoryId :result.category,
						repositoryId : result.repository_mine,
						typicalId:result.typicalId,
						userId :Info.userId
					},
					success: function(data) {
						// console.log(data.objs[0])
						var typeData = data.objs[0];
						var typeHtml = "";
						for (let i = 0; i < typeData.length; i++) {
							typeHtml += 
							'<tr>'+
							'<td> ' + typeData[i].name + ' </td>  <' +
							'td > '+ typeData[i].categoryName +' </td>  <' +
							'td > '+ typeData[i].typicalName +' </td> <' +
							'td > '+ typeData[i].brand +' </td> <' +
							'td > '+ typeData[i].typicalModel +' </td>' +
							'<td > '+ typeData[i].inRepository +' </td>' +
							'</tr>'
						}
						table_tby.innerHTML = typeHtml;
					},
					error: function(e) {
						console.log(e.statusText)
					}
				
				});
				// 出库成功提示框
				layer.open({
					content: '出库成功！',
					btnAlign: 'c',
					anim: 5,
					icon: 6,
					cancel: function() {},
				});
				
			},
			error: function(e) {
				layer.open({
					content: '出库失败！',
					btnAlign: 'c',
					anim: 5,
					icon: 5,
					cancel: function() {
					}
				});
			}
		})
	})
	
});


