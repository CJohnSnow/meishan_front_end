var Tbody = document.getElementById("Tbody")
var unit = document.getElementById("unit")
var UserInfo = app.getUserGlobalInfo();
var roleId = UserInfo.roleId
/**
 * 渲染源仓库和目标仓库的ajax
 */
layui.use(['form'], function() {
	var form = layui.form;
	var category = document.getElementById("category");
	var brand = document.getElementById("brand");
	var typical = document.getElementById("typical");
	var model = document.getElementById("model");
	var sourceRepositoryName = document.getElementById("sourceRepositoryName");
	var targetRepositoryName = document.getElementById("targetRepositoryName");
	var UserInfo = app.getUserGlobalInfo();
	var repository = UserInfo.repositoryList;
	var typeUrl;

	// 获取category的select
	$.ajax({
		url: app.ServerUrl + 'v1/type/getAllCategory',
		type: "get",
		contentType: "application/json;charset=UTF-8",
		dataType: "json",
		async: true,
		headers: {
			"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
		},
		data: {},
		success: function(data) {
			// console.log(data.objs[0])
			var typeData = data.objs[0];
			var typeHtml = '';
			var repositoryHtml = '';
			for (let i = 0; i < typeData.length; i++) {
				typeHtml +=
					'<option value="' + typeData[i].categoryId + '">' +
					typeData[i].categoryName +
					'</option>'
			}
			category.innerHTML = typeHtml;
			form.render("select")
			for (let j = 0; j < repository.length; j++) {
				repositoryHtml +=
					'<option value="' + repository[j].repositoryId + '">' +
					repository[j].repositoryName +
					'</option>'
			}
			sourceRepositoryName.innerHTML = repositoryHtml;
			targetRepositoryName.innerHTML = repositoryHtml;
			form.render("select");
		},
		error: function(e) {
			console.log(e.statusText)
		}

	})


	// category选择后渲染type
	form.on('select(category)', function(data) {
		var managerId = app.getManagerId();
		let SelectValue = data.value;
		localStorage.setItem("OutStorage_categoryId",SelectValue);
		// console.log(SelectValue)
		if (SelectValue == 1) {
			typeUrl = 'v1/type/getTypicalNameByCategoryId/1?managerId=' + managerId;
			Ajax_category(typeUrl);
		} else if (SelectValue == 2) {
			typeUrl = 'v1/type/getTypicalNameByCategoryId/2?managerId=' + managerId;
			Ajax_category(typeUrl);
		} else if (SelectValue == 3) {
			typeUrl = 'v1/type/getTypicalNameByCategoryId/3?managerId=' + managerId;
			Ajax_category(typeUrl);
		}
	});

	// category的ajax
	function Ajax_category(Url) {
		$.ajax({
			url: app.ServerUrl + Url,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs)
				var typeData = data.objs[0];
				var typeHtml = "";
				var typicalNameArray = [];
				for (let i = 0; i < typeData.length; i++) {
					if (typicalNameArray.indexOf(typeData[i]) != -1){
						continue;
					}
					typicalNameArray.push(typeData[i]);
				}
				for (var item in typicalNameArray){
					typeHtml += '<option value="' + typicalNameArray[item]+ '" >' +
						typicalNameArray[item] +
						'</option>'
				}
				brand.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

	// brand选择后渲染model
	form.on('select(brand)', function(data) {
		// console.log(data.value)
		let SelectValue = data.value;
		Ajax_brand(SelectValue);

		// typical选择后渲染model
		form.on('select(typical)', function(data) {

			Ajax_typical(SelectValue);
		});
	});

	// type的ajax
	function Ajax_brand(typicalName) {
		var categoryId = localStorage.getItem("OutStorage_categoryId")
		var managerId = app.getManagerId()
		$.ajax({
			url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
				+ typicalName +"?managerId="+managerId +"&categoryId="+categoryId,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs[0])
				var typeData = data.objs[0];
				var typeHtml = "";
				for (let i = 0; i < typeData.length; i++) {
					typeHtml += '<option value="' + typeData[i].brand + '">' +
						typeData[i].brand +
						'</option>'
				}
				typical.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

	// type的ajax
	function Ajax_typical(typicalName) {
		var categoryId = localStorage.getItem("OutStorage_categoryId")
		var managerId = app.getManagerId()
		$.ajax({
			url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
				+ typicalName +"?managerId="+managerId +"&categoryId="+categoryId,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs[0])
				var typeData = data.objs[0];
				var typeHtml = "";
				for (let i = 0; i < typeData.length; i++) {
					var Arry = typeData[i];
					// console.log(Arry);
					for (let j = 0; j < Arry.modelPo.length; j++) {
						typeHtml += '<option value="' + Arry.modelPo[j].typicalModel + '">' +
							Arry.modelPo[j].typicalModel +
							'</option>'
					}
				}
				model.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}
});
