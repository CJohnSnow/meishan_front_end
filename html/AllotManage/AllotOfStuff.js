layui.use(['laydate', 'form', 'layer'], function() {
	var form = layui.form;
	var layer = layui.layer;
	var table_tby = document.getElementById("table-tby");
	var Tr = document.getElementById("Tr");
	var category = document.getElementById("category");
	var brand = document.getElementById("brand");

	// 源仓库DOM
	var sourceRepoist_div = document.getElementById("sourceRepoist_div");
	var SourceWorkId = document.getElementById("SourceWorkId");
	var sourceRepoist_select_div = document.getElementById("sourceRepoist_select_div");
	var sourceRepoist_select = document.getElementById("sourceRepoist_select");
	var source_button = document.getElementById("source_button");
	// 目标仓库DOM
	var targetRepoist_div = document.getElementById("targetRepoist_div");
	var TargetWorkId = document.getElementById("TargetWorkId");
	var targetRepoist_select_div = document.getElementById("targetRepoist_select_div");
	var targetRepoist_select = document.getElementById("targetRepoist_select");
	var target_button = document.getElementById("target_button");
	var source_left = document.getElementById("source_left");

	// 调拨按钮的DOM
	var Allot_num = document.getElementById("Allot_num");
	var Allot_button = document.getElementById("Allot_button");

	var typical = document.getElementById("typical");
	var model = document.getElementById("model");
	var workId = document.getElementById("workId");
	var typeUrl;

	// 获取category的select
	$.ajax({
		url: app.ServerUrl + 'v1/type/getAllCategory',
		type: "get",
		contentType: "application/json;charset=UTF-8",
		dataType: "json",
		async: true,
		headers: {
			"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
		},
		data: {},
		success: function(data) {
			// console.log(data.objs[0])
			var typeData = data.objs[0];
			var typeHtml = '<option>请选择种类</option>';
			var repositoryHtml = '<option>请选择仓库</option>';

			typeHtml +=
				'<option value="' + typeData[2].categoryId + '">' +
				typeData[2].categoryName +
				'</option>'


			category.innerHTML = typeHtml;
			form.render("select")
		},
		error: function(e) {
			console.log(e.statusText)
		}

	})


	// category选择后渲染type
	form.on('select(category)', function(data) {
		var managerId = app.getManagerId()
		let SelectValue = data.value;
		localStorage.setItem("OutStorage_categoryId",SelectValue);
			typeUrl = 'v1/type/getTypicalNameByCategoryId/3?managerId=' + managerId;
			Ajax_category(typeUrl);

	});

	// category的ajax
	function Ajax_category(Url) {
		$.ajax({
			url: app.ServerUrl + Url,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs)
				var typeData = data.objs[0];
				var typeHtml = "<option>请选择类型</option>";
				var typicalNameArray = [];
				for (let i = 0; i < typeData.length; i++) {
					if (typicalNameArray.indexOf(typeData[i]) != -1){
						continue;
					}
					typicalNameArray.push(typeData[i]);
				}
				for (var item in typicalNameArray){
					typeHtml += '<option value="' + typicalNameArray[item]+ '" >' +
						typicalNameArray[item] +
						'</option>'
				}
				typical.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

	// brand选择后渲染typical
	form.on('select(typical)', function(data) {
		// console.log(data.value)
		let SelectValue = data.value;
		Ajax_typical(SelectValue);

		form.on('select(brand)', function(data) {
			// console.log(data.value)
			// 使用brand的名字来进行渲染model
			var brand = data.value;
			Ajax_brand(SelectValue);

			form.on('select(model)', function(data) {
				// console.log(data)
				var model = data.value;
				localStorage.setItem("Allot_typicalId", model);
				sourceRepoist_div.classList.remove("layui-hide");
			});
		});
	});


	// typical的ajax
	function Ajax_typical(typicalName) {
		var categoryId = localStorage.getItem("OutStorage_categoryId")
		var managerId = app.getManagerId()
		$.ajax({
			url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
				+ typicalName +"?managerId="+managerId +"&categoryId="+categoryId,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs[0])
				var typeData = data.objs[0];
				var typeHtml = "<option>请选择品牌</option>";
				for (let i = 0; i < typeData.length; i++) {
					typeHtml += '<option value="' + typeData[i].brand + '">' +
						typeData[i].brand +
						'</option>'
				}
				brand.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

	// brand的ajax
	function Ajax_brand(typicalName) {
		var categoryId = localStorage.getItem("OutStorage_categoryId")
		var managerId = app.getManagerId()
		$.ajax({
			url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
				+ typicalName +"?managerId="+managerId +"&categoryId="+categoryId,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs[0])
				var typeData = data.objs[0];
				var typeHtml = "<option>请选择型号</option>";
				for (let i = 0; i < typeData.length; i++) {
					var Arry = typeData[i];
					for (let j = 0; j < Arry.modelPo.length; j++) {
						typeHtml += '<option  value="' + Arry.modelPo[j].typicalId + '">' +
							Arry.modelPo[j].typicalModel +
							'</option>'
					}
				}
				model.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

	// 来源仓库查找人员的按钮操作
	form.on("submit(source_button)", function(data) {
		var Info = app.getUserGlobalInfo();
		var workId = data.field.SourceWorkId;
		localStorage.setItem("source_workId", data.field.SourceWorkId);
		$.ajax({
			url: app.ServerUrl + '/v1/repository/getRepositoryInfoByWorkId',
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {
				tempUserId: Info.userId,
				workId: workId
			},
			success: function(data) {
				// console.log(data)
				if (data.resultStatus == 1) {
					var typeData = data.objs[0];
					if (typeData == '') {
						layer.open({
							content: '此人没有仓库！',
							btnAlign: 'c',
							anim: 5,
							icon: 5,
							cancel: function() {}
						});
						var typeHtml = "<option>此人没有仓库!</option>";
						sourceRepoist_select.innerHTML = typeHtml;
						form.render("select")
					} else {

						var typeHtml = "<option>请选择此人的仓库</option>";
						for (let i = 0; i < typeData.length; i++) {

							var Arry = typeData[i];
							typeHtml += '<option value="' + Arry.repositoryId + '">' +
								Arry.repositoryName +
								'</option>'
						}
						sourceRepoist_select.innerHTML = typeHtml;
						form.render("select")
						targetRepoist_div.classList.remove("layui-hide");
						sourceRepoist_select_div.classList.remove("layui-hide");
					}
				} else {
					layer.open({
						content: '查无此人！',
						btnAlign: 'c',
						anim: 5,
						icon: 5,
						cancel: function() {}
					});
				}

			},
			error: function(e) {
				layer.open({
					content: '查无此人！',
					btnAlign: 'c',
					anim: 5,
					icon: 5,
					cancel: function() {}
				});
			}
		})
	})

	// 目标仓库查找人员的按钮操作
	form.on("submit(target_button)", function(data) {
		var Info = app.getUserGlobalInfo();
		var workId = data.field.TargetWorkId;
		$.ajax({
			url: app.ServerUrl + '/v1/repository/getRepositoryInfoByWorkId',
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {
				tempUserId: Info.userId,
				workId: workId
			},
			success: function(data) {
				// console.log(data.objs[0])
				if (data.resultStatus == 1) {
					var typeData = data.objs[0];
					if (typeData == '') {
						layer.open({
							content: '此人没有仓库！',
							btnAlign: 'c',
							anim: 5,
							icon: 5,
							cancel: function() {},
						});
						var typeHtml = "<option>此人没有仓库!</option>";
						targetRepoist_select.innerHTML = typeHtml;
						form.render("select")
					} else {
						var typeHtml = "<option>请选择此人的仓库</option>";
						for (let i = 0; i < typeData.length; i++) {
							var Arry = typeData[i];
							typeHtml += '<option  value="' + Arry.repositoryId + '">' +
								Arry.repositoryName +
								'</option>'

						}
						targetRepoist_select.innerHTML = typeHtml;
						form.render("select");
						targetRepoist_select_div.classList.remove("layui-hide");
						Allot_num.classList.remove("layui-hide");
						Allot_button.classList.remove("layui-hide");
					}
				} else {
					layer.open({
						content: '查无此人！',
						btnAlign: 'c',
						anim: 5,
						icon: 5,
						cancel: function() {}
					});
				}

			},
			error: function(e) {
				layer.open({
					content: '查无此人！',
					btnAlign: 'c',
					anim: 5,
					icon: 5,
					cancel: function() {}
				});
			}
		})
	})

	// sourceRepoist_select的select
	form.on('select(sourceRepoist_select)', function(data) {
		var categoryId = localStorage.getItem("OutStorage_categoryId");
		var typicalId = localStorage.getItem("Allot_typicalId");
		var source_workId = localStorage.getItem("source_workId");
		var Info = app.getUserGlobalInfo();
		$.ajax({
			url: app.ServerUrl + 'v1/repository/getThingsInRepositoryByCondition',
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {
				categoryId: categoryId,
				repositoryId: data.value,
				typicalId: typicalId,
				workId: source_workId
			},
			success: function(data) {
				// console.log(data);
				var sum = 0;
				var typeData = data.objs[0];
				for (let i = 0; i < typeData.length; i++) {
					sum += typeData[i].inRepository
				}
				var html = '来源仓库库存量为:' +
					' <span style="color: red;">' + sum + '</span>'
				source_left.innerHTML = html
			},
			error: function(e) {
				console.log(e.statusText)
			}

		});
	});


	form.on('submit(search)', function(data) {
		// console.log(data.field);
		var result = data.field;
		var Info = localStorage.getItem("userInfo");
		if (result.sourceRepoist_select == '请选择此人的仓库' || result.targetRepoist_select == '请选择此人的仓库'){
			layer.msg('请选择仓库！')
			return
		}
		$.ajax({
			url: app.ServerUrl + 'v1/material/outStorageMaterial/' + result.category,
			type: "post",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: JSON.stringify({
				count: result.Allot_num,
				materialTypicalId: result.typicalId,
				operator: Info.userId,
				sourceRepositoryId: result.sourceRepoist_select,
				targetRepositoryId: result.targetRepoist_select,
				name: result.Allot_batch_name,
			}),
			success: function(data) {
				localStorage.removeItem("Allot_category");
				localStorage.removeItem("Allot_typicalId");
				localStorage.removeItem("Allot_source_userId");
				// console.log(data)
				// 出库成功提示框
				layer.open({
					content: '调拨成功！',
					btnAlign: 'c',
					anim: 5,
					icon: 6,
					yes: function() {
						window.location.reload();
					},
				});
			},
			error: function(e) {
				layer.open({
					content: '调拨失败！',
					btnAlign: 'c',
					anim: 5,
					icon: 5,
					cancel: function() {}
				});
			}
		})
	})

});
