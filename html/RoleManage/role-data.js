//  数据渲染Ajax 

var managerId = app.getManagerId();
var cityRoleId = '';

// 渲染所有管理员的数据
$(document).ready(function () {
  $.ajax({
    url: app.ServerUrl + "v1/role/getSubRolesByManagerId?managerId=" + managerId,
    dataType: "json",
    type: "get",
    async: false,
    data: {},
    headers: {
      "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
    },
    success: function (responseData) {
      dataRender(responseData)
      console.log(responseData)
    },
    error: function () {
      layer.msg('数据请求失败', {
        icon: 5,
        time: 1000
      })
    }
  })

})

// 渲染管理员信息表格函数
function dataRender(responseData) {
  var result = responseData.objs[0];
  // 标题渲染
  var city = $('#city').empty();
  city.append(responseData.objs[1].city + '角色管理');

  var str = '';
  $('#tbody').empty();
  for (i = 0; i < result.length; i++) {
    str += '<tr>' +
        '<td>' + result[i].roleId + '</td>' +
        '<td>' + result[i].role + '</td>' +
        '<td class="td-manage">' +
        '<a title="修改"  onclick="xadmin.open(\'修改角色\',\'role-edit.html?countyRoleId=' + result[i].roleId + '&role=' + result[i].role + '\',500,250)" href="javascript:;">' +
        '<button type="button" class="layui-btn">修改</button>' +
        '</a></td>' +
        '<td class="td-manage">' +
        '<a title="删除" onclick="member_del(this, \'' + result[i].roleId + '\')" href="javascript:;">' +
        '<button type="button" class="layui-btn layui-btn-danger">删除</button>' +
        '</a>' +
        '</td>' +
        '</tr>'
  }

  $('#tbody').append(str);

  cityRoleId = responseData.objs[1].cityRoleId;

}

/*角色-删除*/
function member_del(obj, countyRoleId) {
  layer.confirm('确定要删除吗？', {
    icon: 3,
    title: '提示信息',
    yes: function () {
      $.ajax({
        url: app.ServerUrl + 'v1/role/deleteCountyRoleAndCountyRepositoryByCountyRoleId?countyRoleId=' + countyRoleId,
        type: 'post',
        dataType: 'json',
        data: {},
        async: false,
        success: function () {
          $(obj).parents("tr").remove();
          layer.msg('删除成功！', {
            icon: 1,
            time: 1000
          })
        },
        error: function () {
          layer.msg('删除失败', {
            icon: 5,
            time: 5000
          })
        }
      })
    },
    cancel: function (index) {
      layer.close(index);
    }
  })
}
