var Info = app.getUserGlobalInfo();
var table_tby = document.getElementById("table-tby");
$.ajax({
    url: app.ServerUrl + '\n' +
        'v1/repository/getSelfEliminateRequests',
    type: "get",
    contentType: "application/json;charset=UTF-8",
    dataType: "json",
    async: true,
    headers: {
        "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
    },
    data: {
        userId:Info.userId
    },
    success: function(data) {
        console.log(data)
        var result = data.objs[0];
        var html = '';
        for (let i = 0;i<result.length;i++){
            var singleInfo = result[i];
            if (singleInfo.status == 0){
                var inTime = app.getFormatDate(singleInfo.requestTime);
                html +=

                    `<tr> 
                <td> ${singleInfo.serialNum == null? '/':singleInfo.serialNum} </td> 
                <td> ${singleInfo.categoryName} </td> 
                <td>  ${singleInfo.typicalName}  </td> 
                <td>  ${singleInfo.brand}  </td> 
                <td>  ${singleInfo.typicalModel} </td> 
                <td>  ${inTime}  </td>
                <td style="text-align: center"> 
                <button type="button" class="layui-btn layui-btn-danger layui-btn-sm" onclick="DeleteItem(this,'${singleInfo.saleId}')"> 
                取消 
                </button> 
                </td> 
                </td>
                </tr>`

            }else {
                var inTime = app.getFormatDate(singleInfo.requestTime);
                html += `<tr> 
                <td> ${singleInfo.serialNum == null? '/':singleInfo.serialNum} </td> 
                <td> ${singleInfo.categoryName} </td> 
                <td>  ${singleInfo.typicalName}  </td> 
                <td>  ${singleInfo.brand}  </td> 
                <td>  ${singleInfo.typicalModel} </td> 
                <td>  ${inTime}  </td>
                <td style="text-align: center;color: red"> 
                已通过 !
                </td> 
                </td>
                </tr>`

            }
            table_tby.innerHTML = html;
            }


    },
    error: function(e) {
        console.log(e.statusText)
    }

})

// 删除扫码错误之后的数据
function DeleteItem(e,saleId) {
    $.ajax({
        url: app.ServerUrl + '\n' +
            'v1/repository/cancelEliminateRequest',
        type: "get",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        async: true,
        headers: {
            "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
        },
        data: {
            saleId:saleId
        },
        success: function(data) {
            // console.log(data)
            if (data.resultStatus == 1){
                layui.use(['form', 'layer'], function() {
                    var form = layui.form;
                    var layer = layui.layer;
                    layer.open({
                        content: '取消销库申请成功！',
                        btnAlign: 'c',
                        anim: 5,
                        icon: 6,
                        cancel: function() {}
                    });
                })
            }else {
                layer.open({
                    content: '取消销库申请失败！',
                    btnAlign: 'c',
                    anim: 5,
                    icon: 5,
                    cancel: function() {}
                });
            }
        },
        error: function(e) {
            console.log(e.statusText)
        }

    })
    setTimeout(function () {
        $(e).parent().parent().remove();
    },1000)
}

