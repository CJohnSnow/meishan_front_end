window.index = 0;
var elementById = document.getElementById("serial_number");
var serial_number_arr = [];
elementById.addEventListener("keydown", function (e) {

    var category = JSON.parse(localStorage.getItem("Conditions_category")).category.split(",")[0]
    var typical = JSON.parse(localStorage.getItem("Conditions_typical"))
    var brand = JSON.parse(localStorage.getItem("Conditions_brand"))
    var model = JSON.parse(localStorage.getItem("Conditions_model"))

    var category_class = document.getElementById("category")
    var typical_class = document.getElementById("typical")
    var brand_class = document.getElementById("brand")
    var model_class = document.getElementById("model")


    var text = elementById.value;
    if (e.keyCode == 13 && text != "" && text != undefined && text != null) {
        window.index++;
        serial_number_arr.push(text);
        localStorage.setItem("serial_number_arr", serial_number_arr);
        let html =
            '<td> ' + text + ' </td>  <' +
            'td > ' + category + ' </td>  <' +
            'td > ' + typical.typical + ' </td> <' +
            'td > ' + brand.brand + ' </td> <' +
            'td > ' + model.model + ' </td>' +
            '<td class = ""' +
            'style = "display: flex;justify-content: space-around;" >' +
            '<a title = "删除"' +
            'onclick = "DeleteItem(this)"' +
            'href = "javascript:;" >' +
            '<button class = "layui-btn layui-btn-danger layui-btn-mini"' +
            'onclick = ""> <i class = "layui-icon" > </i>删除</button>' +
            '</a> ' +
            '</td> '

        var the_tr = document.createElement("tr");
        the_tr.innerHTML = html;
        document.getElementById("table-tby").appendChild(the_tr);
        elementById.value = "";
    }
});

// 删除扫码错误之后的数据
function DeleteItem(e) {
    $(e).parent().parent().remove();
}

// 提交扫码后的数据
function InstorageInfo_Ajax() {
    var managerId = app.getManagerId();
    var type = JSON.parse(localStorage.getItem("Conditions_category"));
    var type_num = type.category.split(",");
    var typical = JSON.parse(localStorage.getItem("Conditions_typical"));
    var brand = JSON.parse(localStorage.getItem("Conditions_brand"));
    var model = JSON.parse(localStorage.getItem("Conditions_model"));
    var serial_number = localStorage.getItem("serial_number_arr");
    var materialTypicalId = localStorage.getItem("materialTypicalId");
    var respositoryId = localStorage.getItem("repositoryId");
    var operator = app.getUserGlobalInfo();
    var operator_Id = operator.userId;
    var arr = [];
    var serialNum_arr = [];
    arr.push(serial_number);
    var new_arr = arr[0].split(",");
    for (let i = 0; i < new_arr.length; i++) {
        serialNum_arr.push({
            repositoryId: respositoryId,
            typicalId: materialTypicalId,
            userId: operator_Id,
            serialNum: new_arr[i],
            roleId:operator.pid,
            saleType:type_num[1]
        })
    }
    $.ajax({
        url: app.ServerUrl + 'v1/repository/addEliminateRequest?managerId=' + managerId,
        type: "post",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        async: true,
        data: JSON.stringify(serialNum_arr),
        success: function (data) {
            console.log(data);
            if (data.resultStatus == 1) {
                layer.open({
                    content: '申请销库成功！',
                    btnAlign: 'c',
                    anim: 5,
                    icon: 6,
                    yes: function () {
                        window.location.reload();
                    },
                });
                return;
            }
            if (data.objs != null) {
                var result = data.objs[0];
               {
                    window.PartitionId = result;
                    layer.open({
                        type: 2, //类型
                        title: '错误信息',//窗口名字
                        maxmin: true,
                        shadeClose: false, //点击遮罩不关闭层
                        area: ['1000px', '600px'], //定义宽和高
                        content: 'error.html', //打开的内容
                        cancel:function(){
                            localStorage.removeItem("serial_number_arr")
                            window.location.reload();
                        },
                        success: function (layero, index) {
                            var iframe = window['layui - layer - iframe' + index];
                            // iframe.child(taskid);
                        }
                    })


                }
            } else {
                return
            }
        },
        error: function (e) {
            layer.open({
                content: '申请销库失败！',
                btnAlign: 'c',
                anim: 5,
                icon: 5,
                cancel: function () {

                }
            });
        }

    })
}
