layui.use(['laydate', 'form'], function() {
	var form = layui.form;
	var category = document.getElementById("category");
	var brand = document.getElementById("brand");
	var typical = document.getElementById("typical");
	var model = document.getElementById("model");
	var serial_number = document.getElementById("serial_number");
	var RepositoryDiv = document.getElementById("repository");
	var repository = app.getUserGlobalInfo();
	var repository_List = repository.repositoryList;
	var typeUrl;

	// 获取category的select
	$.ajax({
		url: app.ServerUrl + 'v1/type/getAllCategory',
		type: "get",
		contentType: "application/json;charset=UTF-8",
		dataType: "json",
		async: true,
		headers: {
			"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
		},
		data: {},
		success: function(data) {
			// console.log(data.objs[0])
			var typeData = data.objs[0];
			var typeHtml = '<option>请选择种类</option>';
			var repositoryHtml = '<option>请选择仓库</option>';
			
				typeHtml +=
					'<option value="' + typeData[0].categoryName +','+ typeData[0].categoryId +'">' +
					typeData[0].categoryName +
					'</option>'+
					'<option value="' + typeData[1].categoryName +','+ typeData[1].categoryId +'">' +
					typeData[1].categoryName +
					'</option>'
			
			category.innerHTML = typeHtml;
			for (let i = 0; i < repository_List.length; i++) {
				repositoryHtml +=
					'<option value="' + repository_List[i].repositoryId + '">' +
					repository_List[i].repositoryName +
					'</option>'
			}
			RepositoryDiv.innerHTML = repositoryHtml;
			form.render("select")
		},
		error: function(e) {
			console.log(e.statusText)
		}

	})


	// category选择后渲染type
	form.on('select(category)', function(data) {
		var managerId = app.getManagerId();
		let SelectValue = data.value;
		var SelectValue_arr = SelectValue.split(",");
		var SelectValue_name =SelectValue_arr[0];
		localStorage.setItem("Conditions_category", JSON.stringify({
			'category': SelectValue
		}));
		localStorage.setItem("OutStorage_categoryId",SelectValue_arr[1]);
		// console.log(SelectValue)
		if (SelectValue_name == '终端') {
			typeUrl = 'v1/type/getTypicalNameByCategoryId/1?managerId=' + managerId;
			Ajax_category(typeUrl);
		} else if (SelectValue_name == '装备') {
			typeUrl = 'v1/type/getTypicalNameByCategoryId/2?managerId=' + managerId;
			Ajax_category(typeUrl);
		}
	});

	// category的ajax
	function Ajax_category(Url) {
		$.ajax({
			url: app.ServerUrl + Url,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs)
				var typeData = data.objs[0];
				var typeHtml = "<option>请选择类型</option>";
				var typicalNameArray = [];
				for (let i = 0; i < typeData.length; i++) {
					if (typicalNameArray.indexOf(typeData[i]) != -1){
						continue;
					}
					typicalNameArray.push(typeData[i]);
				}
				for (var item in typicalNameArray){
					typeHtml += '<option value="' + typicalNameArray[item]+ '" >' +
						typicalNameArray[item] +
						'</option>'
				}
				typical.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

	// brand选择后渲染typical
	form.on('select(typical)', function(data) {
		// console.log(data.value)
		let SelectValue = data.value;
		Ajax_typical(SelectValue);
		localStorage.setItem("Conditions_typical", JSON.stringify({
			'typical': SelectValue
		}));

		// typical选择后渲染model
		form.on('select(brand)', function(data) {
			// console.log(data.value)
			// 使用brand的名字来进行渲染model
			var brand = data.value;
			Ajax_brand(SelectValue);
			localStorage.setItem("Conditions_brand", JSON.stringify({
				'brand': brand
			}));
			// model的select
			form.on('select(model)', function(data) {
				// console.log(data)
				var model = data.value;
				var typicalId = app.getCaption(model);
				var model_type = model.split(",");
				localStorage.setItem("Conditions_model",JSON.stringify({'model':model_type[0]}));
				localStorage.setItem("materialTypicalId",typicalId)
			});
		});
	});
	
	// repository的select
	form.on('select(repository)', function(data) {
		// console.log(data)
		serial_number.classList.remove("layui-hide");
		var repository = data.value;
		localStorage.setItem("repositoryId",repository);
	});

	// typical的ajax
	function Ajax_typical(typicalName) {
		var categoryId = localStorage.getItem("OutStorage_categoryId")
		var managerId = app.getManagerId()
		$.ajax({
			url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
				+ typicalName +"?managerId="+managerId +"&categoryId="+categoryId,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs[0])
				var typeData = data.objs[0];
				var typeHtml = "<option>请选择品牌</option>";
				for (let i = 0; i < typeData.length; i++) {
					typeHtml += '<option value="' + typeData[i].brand + '">' +
						typeData[i].brand +
						'</option>'
				}
				brand.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

	// brand的ajax
	function Ajax_brand(typicalName) {
		var categoryId = localStorage.getItem("OutStorage_categoryId")
		var managerId = app.getManagerId()
		$.ajax({
			url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
				+ typicalName +"?managerId="+managerId +"&categoryId="+categoryId,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs[0])
				var typeData = data.objs[0];
				var typeHtml = "<option>请选择型号</option>";
				for (let i = 0; i < typeData.length; i++) {
					var Arry = typeData[i];
					// console.log(Arry);
					for (let j = 0; j < Arry.modelPo.length; j++) {
						typeHtml += '<option x-test= "123" value="' + Arry.modelPo[j].typicalModel +','+Arry.modelPo[j].typicalId+ '">' +
							Arry.modelPo[j].typicalModel +
							'</option>'
					}
				}
				model.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}

		})
	}

});
