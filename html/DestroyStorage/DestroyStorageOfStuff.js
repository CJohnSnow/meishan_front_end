var Tbody = document.getElementById("Tbody")
var unit = document.getElementById("unit")
var UserInfo = app.getUserGlobalInfo();
var roleId = UserInfo.roleId

/**
 * 渲染源仓库和目标仓库的ajax
 */
layui.use(['form'], function() {
	var form = layui.form;
	var category = document.getElementById("category");
	var typical = document.getElementById("typical");
	var RepositoryName = document.getElementById("RepositoryName");
	var UserInfo = app.getUserGlobalInfo();
	var repository = UserInfo.repositoryList;
	// console.log(UserInfo);
	var typeUrl;
	
	// 获取category的select
	$.ajax({
		url: app.ServerUrl + 'v1/type/getAllCategory',
		type: "get",
		contentType: "application/json;charset=UTF-8",
		dataType: "json",
		async: true,
		headers: {
			"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
		},
		data: {},
		success: function(data) {
			// console.log(data.objs[0])
			var typeData = data.objs[0];
			var typeHtml = '<option>请选择</option>';
			var repositoryHtml = '';
		
				typeHtml += 
				'<option value="' + typeData[2].categoryId  + '">' +
					typeData[2].categoryName +
					'</option>'
			
			category.innerHTML = typeHtml;
			form.render("select")
			for(let j=0;j<repository.length;j++){
				repositoryHtml +=
				'<option value="' + repository[j].repositoryId + '">' +
					repository[j].repositoryName +
					'</option>'
			}
			RepositoryName.innerHTML = repositoryHtml;
			form.render("select")
		},
		error: function(e) {
			console.log(e.statusText)
		}
	
	})
	
	
	// category选择后渲染type
	form.on('select(category)', function(data) {
		var managerId = app.getManagerId();
		let SelectValue = data.value;

			typeUrl = 'v1/type/getTypicalByCategoryId/3?managerId=' + managerId;
			Ajax_category(typeUrl);

	});
	
	// category的ajax
	function Ajax_category(Url) {
		$.ajax({
			url: app.ServerUrl + Url,
			type: "get",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			async: true,
			headers: {
				"Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
			},
			data: {},
			success: function(data) {
				// console.log(data.objs)
				var typeData = data.objs[0];
				var typeHtml = "";
				for (let i = 0; i < typeData.length; i++) {
					typeHtml += '<option value="' + typeData[i].typicalId+ '" >' +
						typeData[i].brand + '/'+typeData[i].typicalName+'/'+typeData[i].typicalModel
					'</option>'
				}
				typical.innerHTML = typeHtml;
				form.render("select")
			},
			error: function(e) {
				console.log(e.statusText)
			}
	
		})
	}
});
