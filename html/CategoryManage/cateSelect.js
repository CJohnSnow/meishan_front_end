layui.use(['laydate', 'form', 'layer'], function () {
    var form = layui.form;
    var layer = layui.layer;
    var table_tby = document.getElementById("table-tby");
    var Tr = document.getElementById("Tr");
    var category = document.getElementById("category");
    var brand = document.getElementById("brand");


    var typical = document.getElementById("typical");
    var model = document.getElementById("model");
    var workId = document.getElementById("workId");
    var typeUrl;

    // 获取category的select
    $.ajax({
        url: app.ServerUrl + 'v1/type/getAllCategory',
        type: "get",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        async: true,
        headers: {
            "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
        },
        data: {},
        success: function (data) {
            // console.log(data.objs[0])
            var typeData = data.objs[0];
            var typeHtml = '<option>请选择种类</option>';
            var repositoryHtml = '<option>请选择仓库</option>';
            for (let i = 0; i < typeData.length; i++) {
                typeHtml +=
                    '<option value="' + typeData[i].categoryId + '">' +
                    typeData[i].categoryName +
                    '</option>'
            }
            category.innerHTML = typeHtml;
            form.render("select")
        },
        error: function (e) {
            console.log(e.statusText)
        }

    })

    // category选择后渲染type
    form.on('select(category)', function (data) {
        var managerId = app.getManagerId()
        let SelectValue = data.value;
        localStorage.setItem("OutStorage_categoryId", SelectValue);
        // console.log(SelectValue)
        if (SelectValue == 1) {
            typeUrl = 'v1/type/getTypicalByCategoryId/1?managerId=' + managerId;
            Ajax_category(typeUrl);
        } else if (SelectValue == 2) {
            typeUrl = 'v1/type/getTypicalByCategoryId/2?managerId=' + managerId;
            Ajax_category(typeUrl);
        }else if (SelectValue == 3) {
            typeUrl = 'v1/type/getTypicalByCategoryId/3?managerId=' + managerId;
            Ajax_category(typeUrl);
        }
    });


    // category的ajax
    function Ajax_category(Url) {
        $.ajax({
            url: app.ServerUrl + Url,
            type: "get",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            async: true,
            headers: {
                "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
            },
            data: {},
            success: function (data) {
                // console.log(data.objs)
                var typeData = data.objs[0];
                var typeHtml = "<option>请选择类型</option>";
                var typicalNameArray = [];
                for (let i = 0; i < typeData.length; i++) {
                    if (typicalNameArray.indexOf(typeData[i].typicalName) != -1){
                        continue;
                    }
                    typicalNameArray.push(typeData[i].typicalName);
                }
                for (var item in typicalNameArray){
                    typeHtml += '<option value="' + typicalNameArray[item]+ '" >' +
                        typicalNameArray[item] +
                        '</option>'
                }
                typical.innerHTML = typeHtml;
                form.render("select")
            },
            error: function (e) {
                console.log(e.statusText)
            }

        })
    }

    // brand选择后渲染typical
    form.on('select(typical)', function (data) {
        // console.log(data.value)
        let SelectValue = data.value;
        Ajax_typical(SelectValue);

        form.on('select(brand)', function (data) {
            // console.log(data.value)
            // 使用brand的名字来进行渲染model
            var brand = data.value;
            Ajax_brand(SelectValue);

            form.on('select(model)', function (data) {
                // console.log(data)
                var model = data.value;
            });
        });
    });


    // typical的ajax
    function Ajax_typical(typicalName) {
        var categoryId = localStorage.getItem("OutStorage_categoryId")
        var managerId = app.getManagerId()
        $.ajax({
            url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
                + typicalName + "?managerId=" + managerId + "&categoryId=" + categoryId,
            type: "get",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            async: true,
            headers: {
                "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
            },
            data: {},
            success: function (data) {
                // console.log(data.objs[0])
                var typeData = data.objs[0];
                var typeHtml = "<option>请选择品牌</option>";
                for (let i = 0; i < typeData.length; i++) {
                    typeHtml += '<option value="' + typeData[i].brand + '">' +
                        typeData[i].brand +
                        '</option>'
                }
                brand.innerHTML = typeHtml;
                form.render("select")
            },
            error: function (e) {
                console.log(e.statusText)
            }

        })
    }

    // brand的ajax
    function Ajax_brand(typicalName) {
        var categoryId = localStorage.getItem("OutStorage_categoryId")
        var managerId = app.getManagerId()
        $.ajax({
            url: app.ServerUrl + 'v1/type/getMaterialBrandAndModelPoByTypicalName/'
                + typicalName + "?managerId=" + managerId + "&categoryId=" + categoryId,
            type: "get",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            async: true,
            headers: {
                "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
            },
            data: {},
            success: function (data) {
                // console.log(data.objs[0])
                var typeData = data.objs[0];
                var typeHtml = "<option>请选择型号</option>";
                for (let i = 0; i < typeData.length; i++) {
                    var Arry = typeData[i];
                    for (let j = 0; j < Arry.modelPo.length; j++) {
                        typeHtml += '<option  value="' + Arry.modelPo[j].typicalId + '">' +
                            Arry.modelPo[j].typicalModel +
                            '</option>'
                    }
                }
                model.innerHTML = typeHtml;
                form.render("select")
            },
            error: function (e) {
                console.log(e.statusText)
            }

        })
    }

    form.on('submit(search)', function (data) {
        // console.log(data.field);
        var result = data.field;
        var Info = app.getUserGlobalInfo();
        var categoryId = localStorage.getItem("OutStorage_categoryId")
        $.ajax({
            url: app.ServerUrl + '\n' +
                '/v1/repository/getThingsInRepositoryByCondition',
            type: "get",
            dataType: "json",
            async: true,
            headers: {
                "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
            },
            data: {
                typicalId: result.typicalId,
                categoryId: categoryId
            },
            success: function (res) {
                // console.log(res.objs[0]);
                if (res.objs[0].length == 0) {
                    layer.open({
                        content: '没有查询到相关结果！',
                        btnAlign: 'c',
                        anim: 5,
                        icon: 5,
                        cancel: function () {
                        }
                    });
                } else {
                    layer.open({
                        content: '查询成功！',
                        btnAlign: 'c',
                        anim: 5,
                        icon: 6,
                        cancel: function () {
                        }
                    });
                    var tbody
                    var i = 1
                    res.objs[0].forEach(item => {
                        // console.log(item)
                        var str = `
							  <tr>
								<td>
                                    <input type="checkbox" name="" lay-skin="primary"><div class="layui-unselect layui-form-checkbox" id="select${item.typicalId}" lay-skin="primary"><i class="layui-icon layui-icon-ok" onclick="layuiSelect('${item.typicalId}')"></i></div>
                                  </td>
                                  <td>${i++}</td>
                                  <td>${item.categoryName}</td>
                                  <td>${item.brand}</td>
								  <td>${item.imei == null ? '/':item.imei }</td>
                                  <td>${item.typicalModel}</td>
                                  <td>${item.inRepository == null ? '/':item.inRepository}</td>
								  <td>${item.unit}</td>
                                  <td class="td-manage" style="display: flex;justify-content: space-between;"> 
                                    
                                    <a title="编辑"  onclick="xadmin.open('编辑','admin-edit-fromation.html?${item.typicalId}&${item.typicalName}&${item.brand}&${item.imei}&${item.typicalModel}&${item.unit}')" href="javascript:;">
                                      <i class="layui-icon">&#xe642;</i>
                                      <span>修改信息</span>
                                    </a> 
                                    <a title="删除" onclick="memberdel('${item.id}')">
                                      <i class="layui-icon">&#xe640;</i>
                                      <span>删除</span>
                                    </a>
                                  </td>
                                </tr>
							`
                        tbody += str
                    });
                    $(".name").text("品牌")
                    $(".brand").text("串号")
                    $(".type").text("设备名称")
                    $(".tbody").html(tbody)
                }

            },
            error: function (e) {
                layer.open({
                    content: '查询失败！',
                    btnAlign: 'c',
                    anim: 5,
                    icon: 5,
                    cancel: function () {
                    }
                });
            }
        })
    })

});
