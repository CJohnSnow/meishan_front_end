//  数据渲染Ajax 

var managerId = app.getManagerId();

// 首次自动渲染数据
$(document).ready(function () {
    $.ajax({
        url: app.ServerUrl + "v1/user/selectAllUser?managerId=" + managerId,
        dataType: "json",
        type: "post",
        async: false,
        data: {},
        headers: {
            "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
        },
        success: function (responseData) {
            var result = responseData.objs[0];
            dataRender(result)
        },
        error: function () {
            layer.msg('数据请求失败', {
                icon: 5,
                time: 1000
            })
        }
    })

})

// 渲染用户信息表格函数
function dataRender(result) {
    var str = '';
    $('#tbody').empty();
    for (i = 0; i < result.length; i++) {
        var sex = '';
        if (result[i].sex === "1") {
            sex = '男';
        } else {
            sex = '女';
        }
        str += '<tr>' +
            '<td>' + result[i].workId + '</td>' +
            '<td>' + result[i].username + '</td>' +
            '<td>' + sex + '</td>' +
            '<td>' + result[i].age + '</td>' +
            '<td>' + result[i].role + '</td>' +
            '<td>' + result[i].password + '</td>' +
            '<td class="td-manage">' +
            '<a title="编辑"  onclick="xadmin.open(\'编辑用户信息\',\'user-edit.html?workId=' + result[i].workId + '&roleId=' + result[i].roleId + '\',600,400)" href="javascript:;">' +
            '<button type="button" class="layui-btn">编辑</button>' +
            '</a>&nbsp;&nbsp;' +
            '<a title="删除" onclick="member_del(this, \'' + result[i].userId + '\')" href="javascript:;">' +
            '<button type="button" class="layui-btn layui-btn-danger">删除</button>' +
            '<a title="重置密码" onclick="reset(result[i].userId)" href="javascript:;">' +
            '<button type="button" class="layui-btn layui-btn-danger">重置密码</button>' +
            '</a>' +
            '</td>' +
            '</tr>'
    }
    $('#tbody').append(str);
}

//重置用户密码
function reset(userId) {
    layer.confirm('确定要将密码重置为123456吗？', {
        icon: 3,
        title: '提示信息',
        yes: function () {
            $.ajax({
                url: app.ServerUrl ,
                type: 'post',
                dataType: 'json',
                data: {},
                async: false,
                success: function () {
                    layer.msg('密码重置成功！', {
                        icon: 1,
                        time: 1000
                    })
                },
                error: function () {
                    layer.msg('操作失败', {
                        icon: 5,
                        time: 5000
                    })
                }
            })
        },
        cancel: function (index) {
            layer.close(index);
        }
    })
}

// 根据条件查询用户相应信息

$('#search').click(function () {
    var workId = $('#workId').val().trim();
    var username = $('#username').val().trim();
    if (workId === '' && username === '') {
        layer.alert('查询内容不能为空！', {
            icon: 5,
            time: 1000
        });
    } else {
        var url = '';
        if (workId != '' && username == '') {
            url = app.ServerUrl + "v1/user/queryUser?workId=" + workId;
        } else if (workId == '' && username != '') {
            url = app.ServerUrl + "v1/user/queryUser?username=" + username;
        } else if (workId != '' && username != '') {
            url = app.ServerUrl + "v1/user/queryUser?username=" + username + "&workId=" + workId;
        }
        check(url);
    }
    return false;

})

// 根据条件查询用户相应信息的数据请求
function check(url) {
    $.ajax({
        url: url,
        dataType: "json",
        type: "POST",
        async: false,
        data: {},
        headers: {
            "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
        },
        success: function (responseData) {
            var result = responseData.objs[0];
            if (result == '') {
                layer.msg('查无此人', {
                    icon: 5,
                    time: 1000
                })
            }
            dataRender(result);
        },
        error: function () {
            layer.msg('数据请求失败', {
                icon: 5,
                time: 1000
            })
        }
    })
}

// 渲染用户归属选项
var opitons = "";
$.ajax({
    url: app.ServerUrl + 'v1/role/getSubRolesByManagerId?managerId=' + managerId,
    type: 'get',
    dataType: 'json',
    async: false,
    data: {},
    headers: {
        "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
    },
    success: function (responseData) {
        if (responseData.resultStatus === -1) {
            layer.msg('数据请求失败', {
                icon: 5,
                time: 1000
            })
        }
        var result = responseData.objs[0];

        for (i = 0; i < result.length; i++) {
            opitons += '<option value="' + result[i].roleId + '">' + result[i].role + '</option>';
            opitons += '<option value="' + result[i].roleList[0].roleId + '">' + result[i].roleList[
                0].role + '</option>';
        }
    }
})

/*用户-删除*/
function member_del(obj, userId) {
    $.ajax({
        url: app.ServerUrl + 'v1/user/deleteUserById?managerId=' + managerId + '&userId=' + userId,
        type: 'post',
        dataType: 'json',
        data: {},
        async: false,
        success: function () {
            // layer.confirm("确认要删除吗？",{
            //     btn: ['确认', '取消']
            // }, function () {
            //     $(obj).parents("tr").remove();
            //     layer.msg("删除成功！");
            // }, function(index){
            //     layer.close(index);
            // });

            //   layer.confirm("确认要删除吗？", {
            //       icon: 1,
            //       yes: function(){
            //           $(obj).parents("tr").remove();
            //           layer.msg('删除成功！')
            //       },
            //       cancel: function(index){
            //           layer.close(index);
            //       }
            //   })
        },
        error: function () {
            layer.msg('删除失败', {
                icon: 5,
                time: 1000
            })
        }
    })
}