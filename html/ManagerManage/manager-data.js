//  数据渲染Ajax
var managerId = app.getManagerId();

// 渲染所有管理员的数据
$(document).ready(function () {
    $.ajax({
        url: app.ServerUrl + "v1/user/selectAllManager",
        dataType: "json",
        type: "get",
        async: false,
        data: {},
        headers: {
            "Authorization": "460bcdb0-b326-4d6d-ac99-232c83ba6d80"
        },
        success: function (responseData) {
            var result = responseData.objs[0];
            dataRender(result)
        },
        error: function () {
            layer.msg('数据请求失败', {
                icon: 5,
                time: 1000
            })
        }
    })

})

// 渲染管理员信息表格函数
function dataRender(result) {
    var str = '';
    $('#tbody').empty();
    for (i = 0; i < result.length; i++) {
        var timeLimit = app.getFormatDate(result[i].timeLimit);
        if (result[i].status == 1) {
            str += '<tr>' +
                '<td>' + result[i].workId + '</td>' +
                '<td>' + result[i].manager + '</td>' +
                '<td>' + timeLimit + '</td>' +
                '<td>' + result[i].maxNum + '</td>' +
                '<td>' + result[i].tmpNum + '</td>' +
                '<td class="td-manage">' +
                '<a title="编辑"  onclick="xadmin.open(\'编辑管理员信息\',\'manager-edit.html?managerId=' + result[i].managerId + '\',600,400)" href="javascript:;">' +
                '<button type="button" class="layui-btn">编辑</button>' +
                '</a>&nbsp;&nbsp;' +
                '<a title="禁用" onclick="setStatus(this, \'' + result[i].managerId + '\',\'0\')" href="javascript:;">' +
                '<button type="button" class="layui-btn layui-btn-danger">禁用</button>' +
                '</a>&nbsp;&nbsp;' +
                '<a title="重置密码" onclick="reset(\''+result[i].managerId+'\')" href="javascript:;">' +
                '<button type="button" class="layui-btn layui-btn-normal">重置密码</button>' +
                '</a>' +
                '</td>' +
                '</tr>'
        } else {
            str += '<tr>' +
                '<td>' + result[i].workId + '</td>' +
                '<td>' + result[i].manager + '</td>' +
                '<td>' + timeLimit + '</td>' +
                '<td>' + result[i].maxNum + '</td>' +
                '<td>' + result[i].tmpNum + '</td>' +
                '<td class="td-manage">' +
                '<a title="编辑"  onclick="xadmin.open(\'编辑管理员信息\',\'manager-edit.html?managerId=' + result[i].managerId + '\',600,400)" href="javascript:;">' +
                '<button type="button" class="layui-btn">编辑</button>' +
                '</a>&nbsp;&nbsp;' +
                '<a title="启用" onclick="setStatus(this, \'' + result[i].managerId + '\',\'1\')" href="javascript:;">' +
                '<button type="button" class="layui-btn layui-btn-normal">启用</button>' +
                '</a>&nbsp;&nbsp;' +
                '<a title="重置密码" onclick="reset(\''+result[i].managerId+'\')" href="javascript:;">' +
                '<button type="button" class="layui-btn layui-btn-normal">重置密码</button>' +
                '</a>' +
                '</td>' +
                '</tr>'
        }
    }
    $('#tbody').append(str);
}


//重置管理员密码
function reset(managerId) {
    layer.confirm('确定要将密码重置为123456吗？', {
        icon: 3,
        title: '提示信息',
        yes: function () {
            $.ajax({
                url: app.ServerUrl + 'v1/user/resetManagerPassword',
                type: 'get',
                dataType: 'json',
                data: {
                    managerId:managerId
                },
                async: false,
                success: function () {
                    layer.msg('密码重置成功！', {
                        icon: 1,
                        time: 1000
                    })
                },
                error: function (res) {
                    layer.msg(res.resultIns, {
                        icon: 5,
                        time: 5000
                    })
                }
            })
        },
        cancel: function (index) {
            layer.close(index);
        }
    })
}


/*用户-删除*/
function setStatus(obj, managerId,status) {
    layer.confirm('是否确认当前操作？', {
        icon: 3,
        title: '提示信息',
        yes: function (index) {
            //do something
            $.ajax({
                url: app.ServerUrl + '\n' +
                    'v1/user/setManagerStatusById',
                type: 'get',
                dataType: 'json',
                data: {
                    managerId: managerId,
                    status: status
                },
                async: false,
                success: function (res) {
                    $(obj).parents("tr").remove();
                    if (res.resultStatus == 1){
                        layer.msg('操作成功', {
                            icon: 1,
                            time: 1000
                        })
                        window.location.reload();
                    }else {
                        layer.msg(res.resultIns, {
                            icon: 5,
                            time: 1000
                        })
                    }
                },
                error: function () {
                    layer.msg('操作失败', {
                        icon: 5,
                        time: 1000
                    });
                }
            });

            layer.close(index);
        },
        cancel: function (index) {
            layer.close(index);
            // reload(); // 可以在这里刷新窗口
        }
    })
}